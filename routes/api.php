<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/iss', 'IssController@index')->middleware('auth:api');

// coordinate = "latitude_longitude"
Route::get('/media/{coordinate}', 'MediaController@show')->middleware('auth:api');