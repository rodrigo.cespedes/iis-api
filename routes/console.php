<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('generate-cities-cache', function () {
    $store = Redis::connection('cities');
    $store->flushall();
    \Iss\Repos\Cities\City::chunk(1000, function ($cities) {
        $this->comment("Current Position: " . $cities->first()->id . ".");
        foreach ($cities as $city) {
            event(new \Iss\Events\CityAdded($city));
        }
    });
})->describe('Cycle through every city currently stored in the DB and generate its cache');

Artisan::command('get-media-response-logs', function () {
    $logsCollection = getLogsInFile('GetPictures.txt');
    $logItems = [];
    foreach ($logsCollection as $logs) {
        $logItem = [];
        foreach ($logs as $log) {
            if (!is_array($log)) {
                break;
            }
            if (!array_key_exists('method', $log)) {
                break;
            }
            if ($log['method'] !== 'getImages-response') {
                break;
            }
            $logItem[] = [
                'time' => $log['time'],
                'text' => $log['text']
            ];
        }
        if (count($logItem) === 0) {
            continue;
        }
        $logItems[] = $logItem;
    }
    if (count($logItems) === 0) {
        $this->comment("There have been no attempts to procure images");
        return;
    }
    $index = 0;
    foreach ($logItems as $logItem) {
        $index++;
        $this->comment("(" . strval($index) . ") Image obtained!");
        foreach ($logItem as $item) {
            $this->comment("   - " . $item['text'] . " (" . $item['time'] . ")");
        }
    }
})->describe('Create a print out of image procurement attempts');