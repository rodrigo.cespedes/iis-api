<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {/*
    $city = Iss\Repos\Cities\City::first();
    $repo = new Iss\Repos\Cities\CityRepositoryV1();
    $init1 = microtime(true);
    $cities = $repo->getWithinArea($city->latitude, $city->longitude, 600);
    $total = microtime(true) - $init1;
    echo 'Center point: ' . $city->name . '<br>';
    foreach ($cities as $city) {
        echo $city->name . ' (' . $city->distance . 'kms)<br>';
    }
    echo 'total time: ' . strval($total) . '<br>';*/
    if (is_null(\App\User::first())) {
        return view('auth.register');
    }
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
