### API Description

*ISS-api* tracks the current position of the **I**nternation **S**pace **S**tation (**ISS**).
*ISS-api* uses the latest position acquisition of the **ISS** to procure images or videos of 
cities within a 600km^2 area.

The current **ISS** is fetched from [iss-now.json](http://api.open-notify.org/iss-now.json)

The media is sourced from [pixabay.com](https://pixabay.com/api/docs/)

### Goals of this project
I get passionate about projects and I found that the challenge of resolving cities around a
 geo-position was worth solving. Since solving API requirements isn't part of the scope of the
 code challenge, my initial idea was to create an API and a UI in the same monolithic project.
 I was advised that the evaluation for this project requires that the repo be composed of
 exclusively Front-end technologies; to achieve this, I chose to separate my project into two
 repositories, the API repository ([this repository](https://gitlab.com/rodrigo.cespedes/iis-api))
 and the client repository ([iss-client](https://gitlab.com/rodrigo.cespedes/iss-client)). Since
 this decision wasn't taken at the start of the project, the lack of need for a monolith means 
 that Laravel is no longer the best choice for this project. If I was to start this repository 
 with an API in mind, I would have used Laravel Lumen instead of full Laravel.

### Project Documentation
Please refer to the wiki in order to view progress and docs related to this project:
[Project Wiki](https://gitlab.com/rodrigo.cespedes/iis-api/wikis/home)

### Folks that have visibility on this project
* Ezequiel García (_Senior, Front End Engineer_)
* Leonardo Perez Apiwan (_Lead Software Engineer, Front End_)
* Horacio Oliva (_Director of Engineering_)