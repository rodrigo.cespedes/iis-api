<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Redis;
use Carbon\Carbon;
use Iss\Events\GetIssPosition;
use Iss\Events\TestMediaFreshness;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            echo ' > Check ISS position and cache it -- ' . Carbon::now()->format('d/m/Y H:i:s') . PHP_EOL;
            $this->testForISSPosition();
        })->everyMinute();
        $schedule->call(function () {
            echo ' > Check for outdated links -- ' . Carbon::now()->format('d/m/Y H:i:s') . PHP_EOL;
            $this->pruneOutdatedMedia();
        })->hourly();
    }

    private function testForISSPosition()
    {
        $store = Redis::connection('iss');
        $latestUpdate = $store->get('iss.position.latest'); // in seconds
        if (is_null($latestUpdate)) {
            $latestUpdate = 0;
        } else {
            $latestUpdate = intval($latestUpdate);
        }
        $currentMoment = Carbon::now()->timestamp; // in seconds
        if ($currentMoment - $latestUpdate < 180) {
            // Latest position still fresh, no need to update
            return;
        }
        event(new GetIssPosition());
    }

    private function pruneOutdatedMedia()
    {
        event(new TestMediaFreshness());
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
