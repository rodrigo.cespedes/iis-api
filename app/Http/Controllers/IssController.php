<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IssController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the latest 30 positions of ISS
     *
     * @return array
     */
    public function index()
    {
        $store = \Redis::connection('iss');
        $hashedPositions = $store->get('iss.position.arr');
        if ($hashedPositions) {
            $data = unserialize(base64_decode($hashedPositions));
        } else {
            $data = [];
        }

        $positions = [];
        foreach ($data as $moment => $position) {
            $positions[] = [
                'moment' => intval($moment) * 1000, // JS uses milliseconds
                'latitude' => floatval($position['latitude']),
                'longitude' => floatval($position['longitude']),
                'id' => $position['id']
            ];
        }

        return $positions;
    }
}