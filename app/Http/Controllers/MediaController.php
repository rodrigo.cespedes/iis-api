<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Iss\Repos\Cities\CityInterface;

class MediaController extends Controller
{
    /**
     * Cities Repository
     *
     * @var CityInterface $client
     */
    protected $cities;

    /**
     * Maximum media to return
     *
     * @var integer $maxMedia
     */
    protected $maxMedia;

    /**
     * Create a new controller instance.
     *
     * @param CityInterface $cities
     *
     * @return void
     */
    public function __construct(CityInterface $cities)
    {
        $this->cities = $cities;
        $this->maxMedia = 15;
        $this->middleware('auth');
    }

    /**
     * Get a list of media generated for the supplied coordinates
     *
     * @param string $coordinate - encoded coordinate ("latitude_longitude")
     *
     * @return array
     */
    public function show($coordinate)
    {
        $procuredMedia = [];
        if (is_null($coordinate)) {
            return $procuredMedia;
        }
        $coordinatesRaw = explode('_', $coordinate);
        if (count($coordinatesRaw) !== 2) {
            return $procuredMedia;
        }
        $latitude = parseDouble($coordinatesRaw[0]);
        $longitude = parseDouble($coordinatesRaw[1]);

        // Get list of cities within the correct area
        //$cities = $this->cities->getWithinArea($latitude, $longitude, 200, 100);
        $cities = $this->cities->getWithinRadius($latitude, $longitude, 400, 1000);

        // Let's collect the media
        $allMediaFound = [];
        foreach ($cities as $city) {
            $country = '';
            $countryCode = '';
            $continent = '';
            $continentID = '';
            if (!is_null($city->country)) {
                $country = $city->country->name;
                $countryCode = $city->country->code;
                $continent = $city->country->continent();
                $continentID = $city->country->continent;
            }
            $media = $this->cities->getMedia($city);
            foreach ($media as $mediaItem) {
                $item = [
                    'id' => $mediaItem->id,
                    'large' => $mediaItem->large,
                    'medium' => $mediaItem->medium,
                    'small' => $mediaItem->small,
                    'type' => $mediaItem->type,
                    'city' => [
                        'name' => $city->name,
                        'id' => $city->id
                    ],
                    'country' => [
                        'name' => $country,
                        'id' => $countryCode
                    ],
                    'continent' => [
                        'name' => $continent,
                        'id' => $continentID
                    ],
                    'tags' => []
                ];
                foreach ($mediaItem->tags as $tag) {
                    $item['tags'][] = $tag->name;
                }
                $allMediaFound[] = $item;
            }
        }

        // Let's make sure we return the correct amount of data
        if (count($allMediaFound) > $this->maxMedia) {
            $procuredMedia = $this->filterArray($allMediaFound, $this->maxMedia);
        }

        return $procuredMedia;
    }

    private function filterArray($allMedia, $returnCount)
    {
        $cities = [];
        foreach ($allMedia as $media) {
            if (array_search($media['city']['id'], $cities) === false) {
                $cities[] = $media['city']['id'];
            }
        }
        $maxMediaPerCity = intval(ceil($returnCount / count($cities)));

        $returnMediaIDs = [];
        foreach ($cities as $city) {
            $addCount = 0;
            foreach ($allMedia as $media) {
                if ($media['city']['id'] !== $city) {
                    continue;
                }
                $addCount++;
                $returnMediaIDs[] = $media['id'];
                if ($addCount >= $maxMediaPerCity) {
                    break;
                }
            }
        }
        if (count($returnMediaIDs) < $returnCount) {
            foreach ($allMedia as $media) {
                if (array_search($media['id'], $returnMediaIDs) === false) {
                    $returnMediaIDs[] = $media['id'];
                }
            }
        }

        return $this->generateReturnArr($allMedia, $returnMediaIDs, $returnCount);
    }

    private function generateReturnArr($allMedia, $ids, $count)
    {
        $totalAdded = 0;
        $returnMedia = [];
        foreach ($ids as $id) {
            $totalAdded++;
            foreach ($allMedia as $media) {
                if ($media['id'] === $id) {
                    $returnMedia[] = $media;
                    break;
                }
            }
            if ($totalAdded >= $count) {
                break;
            }
        }

        return $returnMedia;
    }
}