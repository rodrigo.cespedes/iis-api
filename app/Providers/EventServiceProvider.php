<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Iss\Events\CityAdded;
use Iss\Events\GetIssPosition;
use Iss\Events\GetMedia;
use Iss\Events\TestMediaFreshness;
use Iss\Listeners\CacheNewCityToRedis;
use Iss\Listeners\GetISSPositionNow;
use Iss\Listeners\GetPictures;
use Iss\Listeners\GetVideos;
use Iss\Listeners\PruneMedia;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        CityAdded::class => [
            CacheNewCityToRedis::class
        ],
        GetIssPosition::class => [
            GetISSPositionNow::class
        ],
        GetMedia::class => [
            GetPictures::class,
            //GetVideos::class
        ],
        TestMediaFreshness::class => [
            PruneMedia::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
