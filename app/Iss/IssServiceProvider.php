<?php

namespace Iss;

use Illuminate\Support\ServiceProvider;

class IssServiceProvider extends ServiceProvider {
    /**
     * Register the currently enabled implementation.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'Iss\Repos\Cities\CityInterface', 'Iss\Repos\Cities\CityRepositoryV1'
        );
    }
}