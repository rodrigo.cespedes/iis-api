<?php

namespace Iss\Repos\Media;

use Illuminate\Database\Eloquent\Model;
use Iss\Repos\Cities\City;
use Iss\Repos\Tags\Tag;

class Media extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'media';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'large',
        'medium',
        'small',
        'type' // image or video
    ];

    /**
     * Get the city that owns the media.
     */
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    /**
     * Get the tags that belong to the media item.
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'media_tag', 'media_id', 'tag_id');
    }
}