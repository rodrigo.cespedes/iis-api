<?php

namespace Iss\Repos\Cities;

use Illuminate\Database\Eloquent\Collection;

interface CityInterface
{
    /**
     * Get the media that has been procured for this city.
     *
     * @param City $city
     *
     * @return Collection or null
     */
    public function getMedia(City $city);

    /**
     * When new media is procured for a city, we can use this method to associate the
     * new media to the city.
     *
     * @param City $city
     * @param array $media - should have the following format;
     *            $media = [
     *                     ...
     *                     [
     *                         'large' => 'url/to/large/image',
     *                         'medium' => 'url/to/medium/image',
     *                         'small' => 'url/to/small/image',
     *                         'tags' => 'comma, separated, list, of, tags',
     *                         'type' => 'image' // can be 'image' or 'video'
     *                     ],
     *                     ...
     *                 ]
     * @return Collection or null
     */
    public function addMedia(City $city, $media);

    /**
     * Considering the given $latitude and $longitude as the center point of a circle
     * with a radius $radius, return an array of cities within the circle; sort the
     * result by ascending order with respect to distance.
     *
     * @param float $latitude
     * @param float $longitude
     * @param float $radius - Radius to search for cities from the center point in KMs
     * @param integer $limit - Limit cities results to this number (default = 15)
     *
     * @return Collection
     */
    public function getWithinRadius($latitude, $longitude,  $radius, $limit = 15);

    /**
     * Considering the given $latitude and $longitude as the center point of a circle
     * with area $area, return an array of cities within the circle.
     *
     * @param float $latitude
     * @param float $longitude
     * @param float $area - Cities within a circle with an area of $area
     * @param integer $limit - Limit cities results to this number (default = 15)
     *
     * @return Collection
     */
    public function getWithinArea($latitude, $longitude, $area, $limit = 15);
}