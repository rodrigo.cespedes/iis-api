<?php /** @noinspection ALL */

namespace Iss\Repos\Cities;

use Illuminate\Database\Eloquent\Collection;
use Iss\Repos\Media\Media;
use Iss\Repos\Tags\Tag;
use Iss\Repos\Tools\GeoHash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class CityRepositoryV1 implements CityInterface
{
    /**
     * Precision of distances
     *
     * @var integer $precision
     */
    protected $precision;

    /**
     * Maximum number of cities to return
     *
     * @var integer $cap
     */
    protected $cap;

    /**
     * Class that generates and manipulates geoHashes
     *
     * @var GeoHash $geoHash
     */
    protected $geoHash;

    /**
     * Earth's configured radius.
     *
     * @var integer $earthRadius
     */
    protected $earthRadius;

    /**
     * Redis store
     *
     * @var Redis $store
     */
    protected $store;

    /**
     * Create a new CityRepositoryV1 instance.
     * @param integer $resultsDistancePrecision - The precision at which to return the
     *                                            distance the cities are from the reference
     *                                            point.
     * @param integer $resultsCap - The maximum number of cities to return for queries
     *                              (default is 500)
     */
    public function __construct($resultsDistancePrecision = 2, $resultsCap = 1000)
    {
        // Set the Earth radius
        $this->earthRadius = 6378;
        // Set the precision
        $this->precision = $resultsDistancePrecision;
        // Set the results cap
        $this->cap = $resultsCap;
        // Implement geoHash tool
        $this->geoHash = new GeoHash($this->earthRadius);
        // Add connection to Redis
        $this->store = Redis::connection('cities');
    }

    /**
     * Get the media that has been procured for this city.
     *
     * @param City $city
     *
     * @return Collection or null
     */
    public function getMedia(City $city)
    {
        return $city->media;
    }

    /**
     * When new media is procured for a city, we can use this method to associate the
     * new media to the city.
     *
     * @param City $city
     * @param array $media - should have the following format;
 *            $media = [
     *                     ...
     *                     [
     *                         'large' => 'url/to/large/image',
     *                         'medium' => 'url/to/medium/image',
     *                         'small' => 'url/to/small/image',
     *                         'tags' => 'comma, separated, list, of, tags',
     *                         'type' => 'image' // can be 'image' or 'video'
     *                     ],
     *                     ...
     *                 ]
     * @return Collection or null
     */
    public function addMedia(City $city, $media)
    {
        if (is_null($city)) {
            return;
        }
        $newMedia = [];
        foreach ($media as $item) {
            if (!$this->validateUniqueMedia($city, $item['large'])) {
                continue;
            }
            $newMedia[] = $item;
        }

        if (count($newMedia) === 0) {
            return;
        }

        foreach ($newMedia as $mediaItem) {
            $mediaInstance = new Media();
            $mediaInstance->large = $mediaItem['large'];
            $mediaInstance->medium = $mediaItem['medium'];
            $mediaInstance->small = $mediaItem['small'];
            $mediaInstance->type = $mediaItem['type'];
            $mediaInstance->city()->associate($city);
            $mediaInstance->save();
            if (strlen($mediaItem['tags']) > 0) {
                $tags = explode(',', $mediaItem['tags']);
                foreach ($tags as $tag) {
                    $tag = trim($tag);
                    if (strlen($tag) < 3) {
                        continue;
                    }
                    $tagInstance = Tag::where('name', $tag)->first();
                    if (is_null($tagInstance)) {
                        $tagInstance = new Tag();
                        $tagInstance->name = $tag;
                        $tagInstance->save();
                    }
                    $alreadyLinked =
                        DB::table('media_tag')
                            ->where([
                                ['media_id', '=', $mediaInstance->id], ['tag_id', '=', $tagInstance->id],
                            ])->count();
                    if ($alreadyLinked === 0) {
                        $mediaInstance->tags()->attach($tagInstance);
                    }
                }
            }
        }
    }
    private function validateUniqueMedia($city, $largeImgUrl)
    {
        $existingMedia = $city->media;

        if ($existingMedia->count() === 0) {
            return true;
        }
        return $existingMedia->where('large', $largeImgUrl)->first() === null;
    }

    /**
     * Considering the given $latitude and $longitude as the center point of a circle
     * with a radius $radius, return an array of cities within the circle; sort the
     * result by ascending order with respect to distance.
     *
     * @param float $latitude
     * @param float $longitude
     * @param float $radius - Radius to search for cities from the center point in KMs
     * @param integer $limit - Limit cities results to this number (default = 15)
     *
     * @return Collection
     */
    public function getWithinRadius($latitude, $longitude,  $radius, $limit = 15)
    {
        // Verify value types
        $lat = parseDouble($latitude, 10);
        $lng = parseDouble($longitude, 10);
        $rad = parseDouble($radius);

        // Determine the geoHash blocks to analyze
        $cells = $this->geoHash->cellsInArea($lat, $lng, $rad);

        // Based on the matched geoHash blocks, get the cities stored in them
        $matchedCities = [];
        if (!is_null($cells)) {
            $len = $cells['resolution'];
            foreach ($cells['cells'] as $cell) {
                $key = 'cities.cells.' . $len . '.' . $cell;
                $cachedCities = $this->store->hgetall($key);
                $matchedCities = array_replace($matchedCities, $cachedCities);
            }
        }

        // Determine the city IDs and the respective distances that were matched
        $allCityIDs = [];
        $distances = [];
        foreach ($matchedCities as $cityID => $coordinates) {
            $coordinates = unserialize($coordinates);
            $distance =
                $this->getDistance(
                    $lat, $lng, $coordinates['lat'], $coordinates['lng']
                );
            if ($distance <= $radius) {
                $allCityIDs[] = $cityID;
                $distances[$cityID] = $distance;
            }
        }
        asort($distances);

        // Get a final list of city IDs
        $cityIDs = [];
        $total = 0;
        foreach ($distances as $cityID => $distance) {
            $total++;
            if ($total > $limit) {
                break;
            }
            $cityIDs[] = $cityID;
        }

        $cities = City::whereIn('id', $cityIDs)->limit($this->cap)->get();
        foreach ($cities as $city) {
            $city->distance = parseDouble($distances[$city->id], $this->precision);
        }

        return $cities->sortBy('distance');
    }

    /**
     * Considering the given $latitude and $longitude as the center point of a circle
     * with area $area, return an array of cities within the circle.
     *
     * @param float $latitude
     * @param float $longitude
     * @param float $area - Cities within a circle with an area of $area
     * @param integer $limit - Limit cities results to this number (default = 15)
     *
     * @return Collection
     */
    public function getWithinArea($latitude, $longitude, $area, $limit = 15)
    {
        // Determine the radius from the $area
        $radius = sqrt($area / pi());

        // Return the cities within the desired radius
        return $this->getWithinRadius($latitude, $longitude, $radius, $limit);
    }

    /**
     * Use Haversine Formula to determine the distance between the provided coordinates
     *
     * This is not abstracted out to a class due to the fact that the full functionality
     * is simplified to one method. If we add more functionality, then we can send it to
     * a dedicated class in the "Iss\Repos\Tools" namespace.
     *
     * @param $lat1 - as degrees
     * @param $lng1 - as degrees
     * @param $lat2 - as degrees
     * @param $lng2 - as degrees
     *
     * @return double
     */
    private function getDistance($lat1, $lng1, $lat2, $lng2) {
        // Determine the difference between the latitudes and then convert the result to radians
        $dLat = deg2rad($lat2 - $lat1);
        // Determine the difference between the longitudes and then convert the result to radians
        $dLng = deg2rad($lng2 - $lng1);

        // Implement Havesine as specified in
        // https://en.wikipedia.org/wiki/Haversine_formula#Formulation
        $a = sin($dLat/2) * sin($dLat/2);
        $a += cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * sin($dLng/2) * sin($dLng/2);
        $c = 2 * asin(sqrt($a));
        $d = $this->earthRadius * $c;

        return $d;
    }
}