<?php

namespace Iss\Repos\Cities;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'countries';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'name',
        'continent',
        'capital'
    ];

    /**
     * Resource to resolve continent codes
     *
     * @var array
     */
    private $continents = [
        'AF' => 'Africa',
        'AN' => 'Antarctica',
        'AS' => 'Asia',
        'EU' => 'Europe',
        'NA' => 'North America',
        'OC' => 'Australia/Oceania',
        'SA' => 'South America'
    ];

    /**
     * Get the country name.
     *
     * @return string
     */
    public function continent()
    {
        if (!array_key_exists($this->continent, $this->continents)) {
            return '';
        }

        return $this->continents[$this->continent];
    }

    /**
     * Get the cities that belong to this country.
     */
    public function cities()
    {
        return $this->hasMany(City::class, 'country_id');
    }
}
