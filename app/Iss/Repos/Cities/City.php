<?php

namespace Iss\Repos\Cities;

use Illuminate\Database\Eloquent\Model;
use Iss\Repos\Media\Media;

class City extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cities';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'country_id',
        'latitude',
        'longitude'
    ];

    /**
     * Get the City's latitude.
     *
     * @param  string  $value
     * @return string
     */
    public function getLatitudeAttribute($value)
    {
        $float = floatval($value);
        $precision = 10;

        return (double)round($float, $precision, PHP_ROUND_HALF_UP);
    }

    /**
     * Get the City's latitude.
     *
     * @param  string  $value
     * @return string
     */
    public function getLongitudeAttribute($value)
    {
        $float = floatval($value);
        $precision = 10;

        return (double)round($float, $precision, PHP_ROUND_HALF_UP);
    }

    /**
     * Get the country that hosts this city.
     */
    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    /**
     * Get the media for this city.
     */
    public function media()
    {
        return $this->hasMany(Media::class);
    }
}