<?php

namespace Iss\Repos\Tags;

use Illuminate\Database\Eloquent\Model;
use Iss\Repos\Media\Media;

class Tag extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tags';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * Get the media that belong to the tag.
     */
    public function media()
    {
        return $this->belongsToMany(Media::class, 'media_tag', 'tag_id', 'media_id');
    }
}