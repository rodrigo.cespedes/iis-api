<?php

use Illuminate\Support\Facades\Storage;

/** @noinspection PhpFullyQualifiedNameUsageInspection */

/**
 * Append error to $filename
 * @param $filename - The name of the file you want to store to
 * @param $errorContent - The text referencing the error
 * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
 */
function saveLogToFile($filename, $errorContent)
{
    $pixbayLogEnabled = env('PIXBAY_QUERY_LOG', false);
    if ($filename === 'GetPictures.txt' && !$pixbayLogEnabled) {
        return;
    }
    $exists = Storage::disk('local')->exists($filename);
    if ($exists) {
        $currContent = unserialize(base64_decode(Storage::disk('local')->get($filename)));
    } else {
        $currContent = [];
    }
    $currContent[] = $errorContent;
    Storage::disk('local')->put($filename, base64_encode(serialize($currContent)));
}

/** @noinspection PhpFullyQualifiedNameUsageInspection */

/**
 * Get the errors referenced in $filename
 * @param $filename - The name of the file you want to store to
 *
 * @return array
 * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
 */
function getLogsInFile($filename)
{
    $exists = Storage::disk('local')->exists($filename);
    if ($exists) {
        return unserialize(base64_decode(Storage::disk('local')->get($filename)));
    }

    return [];
}

/**
 * Parse a value such that a double is returned.
 * @param $originalValue - The value to parse
 * @param $precision - The number of decimals
 *
 * @return double
 */
function parseDouble($originalValue, $precision = 7)
{
    $float = floatval($originalValue);
    $precision = intval($precision);
    if ($precision < 0 || $precision > 10) {
        $precision = 10;
    }
    return (double)round($float, $precision, PHP_ROUND_HALF_UP);
}

/**
 * Generates a random alphanumeric string
 *
 * @param  int  $length
 * @return string
 */
function generateRandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

/**
 * Generates a random alphanumeric string
 *
 * @param string $url
 * @param string $key
 * @param array $params
 * @return string
 */
function buildPixbayRequestUrl($url, $key, $params)
{
    if (is_null($url) || is_null($key) || is_null($params)) {
        return '';
    }
    $cleanUrl = strval($url);
    if (strlen($cleanUrl) < 7) {
        return '';
    }
    if (!array_key_exists('q', $params)) {
        return '';
    }
    $query = $params['q'];
    if (is_null($query)) {
        return '';
    }
    if (strlen($query) < 3) {
        return '';
    }
    $cleanKey = strval($key);
    if (strlen($cleanKey) === 0) {
        return '';
    }

    $finalUrl = $cleanUrl;
    $finalUrl .= '?key=' . $cleanKey;
    foreach ($params as $param => $criteria) {
        if ($param === 'q') {
            continue;
        }
        $finalUrl .= '&';
        $finalUrl .= $param . '=' . $criteria;
    }
    $finalUrl .= '&q=' . $query;

    return $finalUrl;
}