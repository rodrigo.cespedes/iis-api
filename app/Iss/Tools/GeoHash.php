<?php

namespace Iss\Repos\Tools;

class GeoHash
{
    /**
     * characters for base32 geohash
     *
     * @var $geoBase
     */
    protected $geoBase;

    /**
     * Earth radius
     *
     * @var $radius
     */
    protected $earthRadius;

    /**
     * Assign properties
     * @param integer $earthRadius
     */
    public function __construct($earthRadius = 6378)
    {
        // Base 32 GeoHashes, this is the list of characters to use.
        $this->geoBase = '0123456789bcdefghjkmnpqrstuvwxyz';
        $this->earthRadius = $earthRadius;
    }

    /**
     * Get the hashes within the $radius in question with respect to $lat and $lng.
     *
     * The precision of the hashes will be 4 characters long
     *
     * @param double $lat - Latitude
     * @param double $lng - Longitude
     * @param double $radius - in km
     *
     * @return array
     */
    public function cellsInArea ($lat, $lng, $radius)
    {
        // We must return a square that circumscribes the circle created by $radius

        // Determine resolution
        $resolution = 2;
        if ($radius < 601) {
            $resolution = 3;
        }
        if ($radius < 201) {
            $resolution = 4;
        }

        // Determine km per longitude at $lat
        // estimated radius of earth = 6,378 km
        // Formula: (pi/180) * r_earth * cos(theta)
        $kmPerDegreeLng = (pi() / 180) * $this->earthRadius * cos(deg2rad($lat));
        // kms per degree of latitude is approximately 111km
        $kmPerDegreeLat = 111;

        // Determine amount of degrees to modify for lng and lat
        $lngDegMod = abs($radius / $kmPerDegreeLng);
        $latDegMod = abs($radius / $kmPerDegreeLat);

        // Create the area
        // NW
        $lngNW = $lng - $lngDegMod;
        $latNW = $lat + $latDegMod;
        $nwHash = $this->encode($latNW, $lngNW);
        // NE
        $lngNE = $lng + $lngDegMod;
        $latNE = $latNW;
        $neHash = $this->encode($latNE, $lngNE);
        // SW
        $lngSW = $lngNW;
        $latSW = $lat - $latDegMod;
        $swHash = $this->encode($latSW, $lngSW);
        // SE
        $lngSE = $lngNE;
        $latSE = $latSW;
        $seHash = $this->encode($latSE, $lngSE);

        // Trim hashes to required precision of 4 characters
        $nw = substr($nwHash, 0, $resolution);
        $ne = substr($neHash, 0, $resolution);
        $sw = substr($swHash, 0, $resolution);
        $se = substr($seHash, 0, $resolution);

        //Nw: gbe| NE: u0w| SW: eye| SE: snw
        // Determine the cells
        $cells = [];
        if ($nw === $ne && $nw === $sw) {
            // Only one cell;
            $cells[] = $nw;
            return [
                'resolution' => $resolution,
                'cells' => $cells
            ];
        }
        if ($nw === $sw) {
            // Only one row
            return [
                'resolution' => $resolution,
                'cells' => $this->getCellsBetween($nw, $ne, 'e', $resolution)
            ];
        }
        if ($nw === $ne) {
            // Only one column
            return [
                'resolution' => $resolution,
                'cells' => $this->getCellsBetween($nw, $sw, 's', $resolution)
            ];
        }
        // Determined by an area, let's create an area.
        $leftColumn = $this->getCellsBetween($nw, $sw, 's', $resolution);
        $rightColumn = $this->getCellsBetween($ne, $se, 's', $resolution);
        for ($i = 0; $i < count($leftColumn); $i++) {
            $currRow =
                $this->getCellsBetween($leftColumn[$i], $rightColumn[$i], 'e', $resolution);
            if (is_null($currRow)) {
                return null;
            }
            foreach ($currRow as $item) {
                $cells[] = $item;
            }
        }

        return [
            'resolution' => $resolution,
            'cells' => $cells
        ];
    }

    // Restricted to precision of 4 characters
    private function getCellsBetween($cellA, $cellB, $direction, $resolution)
    {
        if (strlen($cellA) !== $resolution || strlen($cellB) !== $resolution) {
            return null;
        }
        $cells = [$cellA];
        if ($cellA === $cellB) {
            return $cells;
        }
        if ($direction !== 'e' && $direction !== 's') {
            return null;
        }

        $continue = true;
        $currCell = $cellA;
        $loops = 0;
        while ($continue) {
            $loops++;
            $adjacent = $this->adjacent($currCell, $direction);
            $cells[] = $adjacent;
            if ($adjacent === $cellB) {
                return $cells;
            }
            $currCell = $adjacent;
            if ($loops > 100) {
                return null;
            }
        }
        return null;
    }

    /**
     * Encode a geohash based on the provided latitude/longitude to a maximum precision of
     * 12 characters
     *
     * @param double $lat - Latitude
     * @param double $lng - Longitude
     *
     * @return string
     */
    public function encode ($lat, $lng)
    {
        $precision = 12;

        $idx = 0; // index into base32 map
        $bit = 0; // each char holds 5 bits
        $evenBit = true;
        $geohash = '';

        $latMin = -90;
        $latMax = 90;
        $lngMin = -180;
        $lngMax = 180;

        while (strlen($geohash) < $precision) {
            if ($evenBit) {
                // bisect E-W longitude
                $lngMid = ($lngMin + $lngMax) / 2;
                if ($lng >= $lngMid) {
                    $idx = ($idx * 2) + 1;
                    $lngMin = $lngMid;
                } else {
                    $idx = $idx * 2;
                    $lngMax = $lngMid;
                }
            } else {
                // bisect N-S latitude
                $latMid = ($latMin + $latMax) / 2;
                if ($lat >= $latMid) {
                    $idx = ($idx * 2) + 1;
                    $latMin = $latMid;
                } else {
                    $idx = $idx * 2;
                    $latMax = $latMid;
                }
            }
            $evenBit = !$evenBit;

            $bit++;
            if ($bit === 5) {
                // 5 bits gives us a character: append it and start over
                $geohash .= substr($this->geoBase, $idx, 1);
                $bit = 0;
                $idx = 0;
            }
        }

        return $geohash;
    }

    /**
     * Decode geohash to latitude/longitude (location is approximate centre of geohash cell.
     *
     * @param string $geohash
     *
     * @return array
     */
    public function decode ($geohash)
    {
        $bounds = $this->bounds($geohash);
        if (is_null($bounds)) {
            return null;
        }
        // now just determine the centre of the cell

        $latMin = $bounds['sw']['lat'];
        $lngMin = $bounds['sw']['lng'];

        $latMax = $bounds['ne']['lat'];
        $lngMax = $bounds['ne']['lng'];

        // cell centre
        $lat = ($latMin + $latMax) / 2;
        $lng = ($lngMin + $lngMax) / 2;

        // round to close to centre without excessive precision: ⌊2-log10(Δ°)⌋ decimal places
        // lat.toFixed(Math.floor(2-Math.log(latMax-latMin)/Math.LN10));

        $precision = floor(2 - (log($latMax - $latMin) / log(10)));
        $lat = number_format($lat, $precision, '.', '');
        $precision = floor(2 - (log($lngMax - $lngMin) / log(10)));
        $lng = number_format($lng, $precision, '.', '');

        return [
            'lat' => floatval($lat),
            'lng' => floatval($lng)
        ];
    }

    /**
     * Get all 8 adjacent cells to the specified $geohash
     *
     * @param string $geohash - The source hash
     *
     * @return array - all 8 neighbors
     */
    public function neighbours ($geohash)
    {
        return [
            'n'  => $this->adjacent($geohash, 'n'),
            'ne' => $this->adjacent($this->adjacent($geohash, 'n'), 'e'),
            'e'  => $this->adjacent($geohash, 'e'),
            'se' => $this->adjacent($this->adjacent($geohash, 's'), 'e'),
            's'  => $this->adjacent($geohash, 's'),
            'sw' => $this->adjacent($this->adjacent($geohash, 's'), 'w'),
            'w'  => $this->adjacent($geohash, 'w'),
            'nw' => $this->adjacent($this->adjacent($geohash, 'n'), 'w'),
        ];
    }

    /**
     * Returns SW/NE latitude/longitude bounds of specified geohash.
     *
     * @param string $geohash
     *
     * @return array
     */
    private function bounds ($geohash) {
        if (strlen($geohash) === 0) {
            return null;
        }
        $geohash = strtolower($geohash);

        $evenBit = true;
        $latMin =  -90;
        $latMax =  90;
        $lngMin = -180;
        $lngMax = 180;

        for ($i = 0; $i < strlen($geohash); $i++) {
            $chr = substr($geohash, $i, 1);
            $idx = strpos($this->geoBase, $chr);
            if (!$idx && $idx !== 0) {
                return null;
            }

            for ($n = 4; $n >= 0; $n--) {
                $bitN = $idx >> $n & 1;
                if ($evenBit) {
                    // longitude
                    $lngMid = ($lngMin + $lngMax) / 2;
                    if ($bitN === 1) {
                        $lngMin = $lngMid;
                    } else {
                        $lngMax = $lngMid;
                    }
                } else {
                    // latitude
                    $latMid = ($latMin + $latMax) / 2;
                    if ($bitN == 1) {
                        $latMin = $latMid;
                    } else {
                        $latMax = $latMid;
                    }
                }
                $evenBit = !$evenBit;
            }
        }

        $bounds = [
            'sw' => [
                'lat' => $latMin,
                'lng' => $lngMin
            ],
            'ne' => [
                'lat' => $latMax,
                'lng' => $lngMax,
            ]
        ];

        return $bounds;
    }

    /**
     * Get an adjacent cell based on the specified $direction
     *
     * @param string $geohash - The source hash
     * @param string $direction - Direction from geohash (N/S/E/W)
     *
     * @return string - geohash of the adjacent cell
     */
    private function adjacent ($geohash, $direction)
    {
        $geohash = strtolower($geohash);
        $direction = strtolower($direction);

        if (strlen($geohash) === 0) {
            return null;
        }

        $match = strpos('nsew', $direction);
        if (!$match && $match !== 0) {
            return null;
        }

        $neighbour = [
            'n' => [ 'p0r21436x8zb9dcf5h7kjnmqesgutwvy', 'bc01fg45238967deuvhjyznpkmstqrwx' ],
            's' => [ '14365h7k9dcfesgujnmqp0r2twvyx8zb', '238967debc01fg45kmstqrwxuvhjyznp' ],
            'e' => [ 'bc01fg45238967deuvhjyznpkmstqrwx', 'p0r21436x8zb9dcf5h7kjnmqesgutwvy' ],
            'w' => [ '238967debc01fg45kmstqrwxuvhjyznp', '14365h7k9dcfesgujnmqp0r2twvyx8zb' ]
        ];

        $border = [
            'n' => [ 'prxz',     'bcfguvyz' ],
            's' => [ '028b',     '0145hjnp' ],
            'e' => [ 'bcfguvyz', 'prxz'     ],
            'w' => [ '0145hjnp', '028b'     ]
        ];

        $lastCh = substr($geohash, -1); // last character of hash
        $parent = substr($geohash, 0, strlen($geohash) - 1); // hash without last character

        $type = strlen($geohash) % 2;

        // check for edge-cases which don't share common prefix
        $edgeMatch = strpos($border[$direction][$type], $lastCh);
        $doNotTestEdge = !$edgeMatch && $edgeMatch !== 0;
        if (!$doNotTestEdge && $parent !== '') {
            // Test edge case
            $parent = $this->adjacent($parent, $direction);
        }

        // append letter for direction to parent
        $neighbourChar = strpos($neighbour[$direction][$type], $lastCh);
        return $parent . substr($this->geoBase, $neighbourChar, 1);
    }
}