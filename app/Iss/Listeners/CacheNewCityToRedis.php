<?php

namespace Iss\Listeners;

use Iss\Events\CityAdded;
use Iss\Repos\Tools\GeoHash;
use Illuminate\Support\Facades\Redis;

class CacheNewCityToRedis
{
    /**
     * Class that generates and manipulates geoHashes
     *
     * @var GeoHash $geoHash
     */
    protected $geoHash;

    /**
     * Redis Cities store
     *
     * @var Redis $store
     */
    protected $store;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->geoHash = new GeoHash();
        $this->store = Redis::connection('cities');
    }

    /**
     * Handle the event.
     *
     * @param  CityAdded  $event
     * @return void
     */
    public function handle(CityAdded $event)
    {
        $hash = $this->geoHash->encode($event->city->latitude, $event->city->longitude);
        $cells = [
            [
                'len' => '2',
                'key' => substr($hash, 0, 2)
            ],
            [
                'len' => '3',
                'key' => substr($hash, 0, 3)
            ],
            [
                'len' => '4',
                'key' => substr($hash, 0, 4)
            ]
        ];
        foreach ($cells as $cell) {
            $key = 'cities.cells.' . $cell['len'] . '.' . $cell['key'];
            $value = [
                'lat' => $event->city->latitude,
                'lng' => $event->city->longitude,
                'hash' => $hash
            ];
            $this->store->hset($key, $event->city->id, serialize($value));
        }
    }
}