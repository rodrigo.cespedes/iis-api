<?php /** @noinspection ALL */

namespace Iss\Listeners;

use Carbon\Carbon;
use GuzzleHttp\Exception\RequestException;
use Iss\Events\GetMedia;
use Iss\Repos\Cities\City;
use Iss\Repos\Cities\CityInterface;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Redis;
use Iss\Events\GetIssPosition;

class GetPictures
{
    /**
     * Store logs here
     *
     * @var array $log
     */
    protected $log;

    /**
     * Guzzle client
     *
     * @var Client $client
     */
    protected $client;

    /**
     * Cities Repository
     *
     * @var CityInterface $client
     */
    protected $cities;

    /**
     * Include the country name in the query
     *
     * @var bool $includeCountryInQuery
     */
    protected $includeCountryInQuery;

    /**
     * Create the event listener.
     *
     * @param CityInterface $cities
     *
     * @return void
     */
    public function __construct(CityInterface $cities)
    {
        // Set log messages to blank
        $this->log = [];

        // Set up the Guzzle client
        $this->client = new Client();

        // Set up the cities repository
        $this->cities = $cities;

        // Set preference for inclusion of country in the query.
        $this->includeCountryInQuery = env('PIXBAY_QUERY_WITH_COUNTRY', false);
    }

    /**
     * Handle the event.
     *
     * @param  GetMedia  $event
     * @return void
     */
    public function handle(GetMedia $event)
    {
        $this->log[] = [
            'method' => 'handle',
            'time' => Carbon::now()->format('d/m/Y H:i:s'),
            'text' => 'Try To get images...'
        ];
        // Make sure we should do the search.
        if (!$event->search) {
            $this->log[] = [
                'method' => 'handle',
                'time' => Carbon::now()->format('d/m/Y H:i:s'),
                'text' => 'Search was blocked due to incorrect parameters'
            ];
            saveLogToFile('GetPictures.txt', $this->log);
            return;
        }
        // Get list of cities within the correct radius
        $cities = $this->cities->getWithinRadius($event->latitude, $event->longitude, 1000, 3000);
        $this->log[] = [
            'method' => 'handle',
            'time' => Carbon::now()->format('d/m/Y H:i:s'),
            'text' => 'Search for cities around lat: ' . strval($event->latitude) . ', lng: ' . strval($event->longitude)
        ];
        if ($cities->count() === 0) {
            $this->log[] = [
                'method' => 'handle',
                'time' => Carbon::now()->format('d/m/Y H:i:s'),
                'text' => 'No cities found in a 2000km radius around the coordinates.'
            ];
            saveLogToFile('GetPictures.txt', $this->log);
            return;
        }
        $this->log[] = [
            'method' => 'handle',
            'time' => Carbon::now()->format('d/m/Y H:i:s'),
            'text' => strval($cities->count()) . ' cities found!'
        ];
        $cityCnt = 0;
        foreach ($cities as $city) {
            if ($this->cities->getMedia($city)->count() > 2) {
                continue;
            }

            if ($cityCnt > 150) {
                return;
            }
            // Build the query
            if ($this->includeCountryInQuery) {
                $country = $city->country;
                if (is_null($country)) {
                    continue;
                }
                if (strlen($country) === 0) {
                    continue;
                }
                $country = strtolower(trim($country->name));
                $country = str_replace(' ', '+', $country);
            } else {
                $country = '';
            }

            $name = strtolower(trim($city->name));
            if (strlen($name) === 0) {
                continue;
            }
            $cityCnt++;
            $name = str_replace(' ', '+', $name);
            if ($this->includeCountryInQuery) {
                $query = $country . '+' . $name;
            } else {
                $query = $name;
            }

            // Build url
            $params = $event->params;
            $params['q'] = $query;
            $url = buildPixbayRequestUrl($event->imgUrl, $event->apiKey, $params);
            if (strlen($url) === 0) {
                return;
            }
            $this->getImages($url, $city);
        }
    }

    private function getImages($url, $city)
    {
        $this->log[] = [
            'method' => 'getImages',
            'time' => Carbon::now()->format('d/m/Y H:i:s'),
            'text' => 'Url is: ' . $url
        ];
        saveLogToFile('GetPictures.txt', $this->log);
        $request = new Request('GET', $url);
        $promise = $this->client->sendAsync($request)->then(
            function (ResponseInterface $response) use ($city) {
                // Set up log for response
                $log = [];

                // Initial validations
                if ($response->getStatusCode() !== 200) {
                    return;
                }
                /** @noinspection PhpComposerExtensionStubsInspection */
                $body = json_decode((string) $response->getBody(), true);
                if (!array_key_exists('totalHits', $body)) {
                    return;
                }
                if ($body['totalHits'] == 0) {
                    $log[] = [
                        'method' => 'getImages-response',
                        'time' => Carbon::now()->format('d/m/Y H:i:s'),
                        'text' => 'No hits (A)!'
                    ];
                    saveLogToFile('GetPictures.txt', $log);
                    return;
                }
                if (!array_key_exists('hits', $body)) {
                    $log[] = [
                        'method' => 'getImages-response',
                        'time' => Carbon::now()->format('d/m/Y H:i:s'),
                        'text' => 'No hits (B)!'
                    ];
                    saveLogToFile('GetPictures.txt', $log);
                    return;
                }
                $body = $body['hits'];

                // Gather required data
                $media = [];
                foreach ($body as $hit) {
                    if (!array_key_exists('largeImageURL', $hit)) {
                        return;
                    }
                    if (!array_key_exists('webformatURL', $hit)) {
                        return;
                    }
                    if (!array_key_exists('previewURL', $hit)) {
                        return;
                    }
                    if (!array_key_exists('tags', $hit)) {
                        return;
                    }
                    $media[] = [
                        'large' => $hit['largeImageURL'],
                        'medium' => $hit['webformatURL'],
                        'small' => $hit['previewURL'],
                        'tags' => $hit['tags'],
                        'type' => 'image'
                    ];
                }
                if (count($media) === 0) {
                    $log[] = [
                        'method' => 'getImages-response',
                        'time' => Carbon::now()->format('d/m/Y H:i:s'),
                        'text' => 'No hits (C)!'
                    ];
                    saveLogToFile('GetPictures.txt', $log);
                    return;
                }
                $log[] = [
                    'method' => 'getImages-response',
                    'time' => Carbon::now()->format('d/m/Y H:i:s'),
                    'text' => 'Total successful hits: ' . strval(count($media))
                ];
                saveLogToFile('GetPictures.txt', $log);
                $this->cities->addMedia($city, $media);
            },
            function (RequestException $e) {
                echo $e->getMessage() . PHP_EOL;
                echo $e->getRequest()->getMethod();
            }
        );
        $promise->wait();
    }
}