<?php

namespace Iss\Listeners;

use Carbon\Carbon;
use Iss\Events\TestMediaFreshness;
use Illuminate\Support\Facades\DB;
use Iss\Repos\Media\Media;

class PruneMedia
{
    /**
     * Handle the event.
     *
     * @param  TestMediaFreshness  $event
     * @return void
     */
    public function handle(TestMediaFreshness $event)
    {
        // Determine IDs to delete; it's good to determine if anything needs to be
        // deleted because querying with the model is expensive
        $referenceDate = Carbon::now()->subMinutes($event->lifetime);
        $availableForDelte =
            DB::table('media')->select('id')
                ->whereDate('created_at','<=', $referenceDate)
                ->count();
        if ($availableForDelte === 0) {
            return;
        }
        echo ' > Delete stale image links -- ' . Carbon::now()->format('d/m/Y H:i:s') . PHP_EOL;
        Media::where('created_at','<=', $referenceDate)->chunk(500, function ($media) {
            foreach ($media as $media) {
                $media->delete();
            }
        });
    }
}
