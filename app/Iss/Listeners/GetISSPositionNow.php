<?php

namespace Iss\Listeners;

use Carbon\Carbon;
use GuzzleHttp\Exception\RequestException;
use Iss\Events\GetMedia;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Redis;
use Iss\Events\GetIssPosition;

class GetISSPositionNow
{
    /**
     * Guzzle client
     *
     * @var Client $client
     */
    protected $client;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * Handle the event.
     *
     * @param  GetIssPosition  $event
     * @return void
     */
    public function handle(GetIssPosition $event)
    {
        $request = new Request('GET', $event->url);
        $promise = $this->client->sendAsync($request)->then(
            function (ResponseInterface $response) {
                if ($response->getStatusCode() !== 200) {
                    return;
                }
                /** @noinspection PhpComposerExtensionStubsInspection */
                $body = json_decode((string) $response->getBody(), true);
                if (!array_key_exists('iss_position', $body)) {
                    return;
                }
                if (!array_key_exists('latitude', $body['iss_position'])) {
                    return;
                }
                if (!array_key_exists('longitude', $body['iss_position'])) {
                    return;
                }
                // Make connection to store
                $store = Redis::connection('iss');

                // Get the current store
                $currentArr = [];
                $currentHash = $store->get('iss.position.arr');
                $data = null;
                if ($currentHash) {
                    $data = unserialize(base64_decode($currentHash));
                }
                if ($data) {
                    $currentArr = $data;
                }

                // Organize DB descending
                krsort($currentArr);

                // Update cache such that the latest procured time is stored
                $currentMoment = Carbon::now()->timestamp;
                $store->set('iss.position.latest', $currentMoment);
                // Set up new registry for the latest position
                $latestPos = [
                    'id' => generateRandomString(15),
                    'latitude' => floatval($body['iss_position']['latitude']),
                    'longitude' => floatval($body['iss_position']['longitude'])
                ];

                // Create final array
                $finalArr = [];
                $finalArr[$currentMoment] = $latestPos;
                $count = 0;
                foreach ($currentArr as $moment => $currPos) {
                    $count++;
                    if ($count >= 30) {
                        break;
                    }
                    $finalArr[$moment] = $currPos;
                }
                $store->set('iss.position.arr', base64_encode(serialize($finalArr)));

                // Create a store of images for the latest coordinates
                event(new GetMedia($latestPos));
            },
            function (RequestException $e) {
                echo $e->getMessage() . PHP_EOL;
                echo $e->getRequest()->getMethod();
            }
        );
        $promise->wait();
    }
}