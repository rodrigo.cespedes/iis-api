<?php

namespace Iss\Listeners;

use Carbon\Carbon;
use GuzzleHttp\Exception\RequestException;
use Iss\Events\GetMedia;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Redis;
use Iss\Events\GetIssPosition;

class GetVideos
{
    /**
     * Guzzle client
     *
     * @var Client $client
     */
    protected $client;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * Handle the event.
     *
     * @param  GetMedia  $event
     * @return void
     */
    public function handle(GetMedia $event)
    {
        // Make sure we should do the search.
        if (!$event->search) {
            return;
        }

        // Get list of cities within the correct radius
        $cities = $this->cities->getWithinArea($event->latitude, $event->longitude, 200);

        // Build url

    }
}