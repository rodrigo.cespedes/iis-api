<?php

namespace Iss\Events;

use Illuminate\Queue\SerializesModels;

class GetMedia
{
    use SerializesModels;

    /**
     * Pixbay API key
     *
     * @var string
     */
    public $apiKey;

    /**
     * Petition parameters
     *
     * @var string
     */
    public $params;

    /**
     * Request url for images
     *
     * @var string
     */
    public $imgUrl;

    /**
     * Request url for videos
     *
     * @var string
     */
    public $vidUrl;

    /**
     * Search coordinates (latitude)
     *
     * @var float
     */
    public $latitude;

    /**
     * Search coordinates (longitude)
     *
     * @var float
     */
    public $longitude;

    /**
     * If there is an issue with the supplied data, this will be set to false to halt
     * procurement of data.
     *
     * @var boolean
     */
    public $search;

    /**
     * Create a new event instance.
     *
     * @param array $coordinates - coordinates to base the search criteria on
     *
     * @return void
     */
    public function __construct($coordinates)
    {
        // Validate supplied array
        if (!array_key_exists('latitude', $coordinates)) {
            $this->search = false;
            return;
        }
        if (!array_key_exists('longitude', $coordinates)) {
            $this->search = false;
            return;
        }

        // Set up coordinates
        $latitude = $coordinates['latitude'];
        if (is_null($latitude)) {
            $this->search = false;
            return;
        }
        $latitude = floatval($latitude);
        $this->latitude = $latitude;
        $longitude = $coordinates['longitude'];
        if (is_null($longitude)) {
            $this->search = false;
            return;
        }
        $longitude = floatval($longitude);
        $this->longitude = $longitude;

        // Search is valid, document it as valid
        $this->search = true;
        // Set the API key
        $this->apiKey = env('PIXBAY_KEY', 'PermisoGlobal');
        $this->params = [
            'category' => 'places',
            'safesearch' => 'true',
            'order' => 'popular',
            'page' => '1',
            'per_page' => '30'
        ];
        // API images url
        $this->imgUrl = 'https://pixabay.com/api/';
        // API videos url
        $this->vidUrl = 'https://pixabay.com/api/videos';
    }
}