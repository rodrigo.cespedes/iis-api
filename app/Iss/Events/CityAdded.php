<?php

namespace Iss\Events;

use Illuminate\Queue\SerializesModels;
use Iss\Repos\Cities\City;

class CityAdded
{
    use SerializesModels;

    /**
     * Service that was created
     *
     * @var City $city
     */
    public $city;

    /**
     * Create a new event instance.
     *
     * @param $city - Instance of newly added city
     *
     * @return void
     */
    public function __construct(City $city)
    {
        $this->city = $city;
    }
}