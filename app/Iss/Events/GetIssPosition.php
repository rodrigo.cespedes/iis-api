<?php

namespace Iss\Events;

use Illuminate\Queue\SerializesModels;

class GetIssPosition
{
    use SerializesModels;

    /**
     * ISS position API url
     *
     * @var string
     */
    public $url;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct()
    {
        // API url
        $this->url = 'http://api.open-notify.org/iss-now.json';
    }
}