<?php

namespace Iss\Events;

use Illuminate\Queue\SerializesModels;

class TestMediaFreshness
{
    /**
     * Media lifetime
     *
     * @var integer $lifetime - lifetime measured in minutes
     */
    public $lifetime;

    /**
     * Check for media to delete and specify media lifetime
     *
     * @return void
     */
    public function __construct()
    {
        $this->lifetime = 180;
    }
}