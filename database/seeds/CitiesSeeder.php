<?php

use Illuminate\Database\Seeder;

require_once (base_path('database/seeds/citiesList.php'));

class CitiesSeeder extends Seeder
{
    /**
     * Run the database cities seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = date('Y-m-d');
        $countries = DB::table('countries')->get();
        $cities = getCities();
        foreach ($cities as $city) {
            $code = $city['country'];
            $country = $countries->where('code', '=', $code)->first();
            if (is_null($country)) {
                continue;
            }
            DB::table('cities')->insert([
                'name' => trim($city['name']),
                'country_id' => $country->id,
                'latitude' => $city['latitude'],
                'longitude' => $city['longitude'],
                'created_at' => $date,
                'updated_at' => $date
            ]);
        }
    }
}
