<?php

use Illuminate\Database\Seeder;

require_once (base_path('database/seeds/countriesList.php'));

class CountriesSeeder extends Seeder
{
    /**
     * Run the database cities seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = date('Y-m-d');
        $countries = getCountries();
        foreach ($countries as $country) {
            DB::table('countries')->insert([
                'code' => $country['code'],
                'name' => trim($country['name']),
                'continent' => $country['continent'],
                'capital' => trim($country['capital']),
                'created_at' => $date,
                'updated_at' => $date
            ]);
        }
    }
}
