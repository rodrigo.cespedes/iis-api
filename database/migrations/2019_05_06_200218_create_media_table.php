<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('city_id')->unsigned();
            $table->string('large');
            $table->string('medium');
            $table->string('small');
            $table->string('type');
            $table->timestamps();

            $table->foreign('city_id')->references('id')->on('cities');
        });

        // tags
        Schema::create('tags', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->timestamps();
        });

        // Intermediate table
        Schema::create('media_tag', function(Blueprint $table) {
            $table->bigInteger('media_id')->unsigned();
            $table->bigInteger('tag_id')->unsigned();

            $table->foreign('media_id')
                ->references('id')
                ->on('media')
                ->onDelete('cascade');
            $table->foreign('tag_id')
                ->references('id')
                ->on('tags')
                ->onDelete('cascade');

            $table->primary(['media_id', 'tag_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media_tag');
        Schema::dropIfExists('media');
        Schema::dropIfExists('tags');
    }
}
